import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import pickle
import scipy
# from combined_thresh import combined_thresh
# from perspective_transform import perspective_transform
# from line_profiler import LineProfiler

# feel free to adjust the parameters in the code if necessary

def show(img):
	plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
	plt.show()

def estimate_right_lane(left_fit, distance):
	right_fit = left_fit.copy()
	right_fit[-1] += distance
	return right_fit

def estimate_left_lane(right_fit, distance):
	left_fit = right_fit.copy()
	left_fit[-1] -= distance
	return left_fit

def curvature(x, y):
    """
    Calculate the curvature of a curve represented by a list of 2D points.
    
    Args:
        points (list of tuples): List of 2D points [(x1, y1), (x2, y2), ...]
    
    Returns:
        list of floats: List of curvatures at each point
    """
    num_points = len(x)
    if num_points < 3:
        raise ValueError("At least 3 points are required to calculate curvature")

    # x = np.array([point[0] for point in points])
    # y = np.array([point[1] for point in points])

    dx_dt = np.gradient(x)
    dy_dt = np.gradient(y)
    d2x_dt2 = np.gradient(dx_dt)
    d2y_dt2 = np.gradient(dy_dt)

    curvature_values = np.abs(d2x_dt2 * dy_dt - dx_dt * d2y_dt2) / (dx_dt**2 + dy_dt**2)**1.5

    mean_curvature = np.mean(curvature_values)

    return mean_curvature

def line_fit(binary_warped, plot=False):
	"""
	Find and fit lane lines
	"""
	# print("fsfdsddffdsfd")
	# Assuming you have created a warped binary image called "binary_warped"
	# Take a histogram of the bottom half of the image
	histogram = np.sum(binary_warped[binary_warped.shape[0]//2:,:], axis=0)
	# plt.plot(histogram)
	# plt.show()
	# Create an output image to draw on and visualize the result
	out_img = (np.dstack((binary_warped, binary_warped, binary_warped))*255).astype('uint8')
	# print(histogram.shape)
	# Find the peak of the left and right halves of the histogram
	# These will be the starting point for the left and right lines
	midpoint = int(histogram.shape[0]/2)
	leftx_base = np.argmax(histogram[100:midpoint]) + 100
	rightx_base = np.argmax(histogram[midpoint:-100]) + midpoint

	# Choose the number of sliding windows
	nwindows = 20
	# Set height of windows
	window_height = int(binary_warped.shape[0]/nwindows)
	window_width = int(binary_warped.shape[1])
	# Identify the x and y positions of all nonzero pixels in the image
	nonzero = binary_warped.nonzero()
	# print(nonzero)
	nonzeroy = np.array(nonzero[0])
	nonzerox = np.array(nonzero[1])
	# print(nonzerox)
	# Current positions to be updated for each window
	leftx_current = leftx_base
	rightx_current = rightx_base
	# Set the width of the windows +/- margin
	margin = 150
	# Set minimum number of pixels found to recenter window
	minpix = 50
	# Create empty lists to receive left and right lane pixel indices
	left_lane_inds = []
	right_lane_inds = []
	stop_left = 1
	stop_right = 1
	threshold = .7
	# Step through the windows one by one
	for window in range(nwindows, -1, -1):
		# Identify window boundaries in x and y (and right and left)
		##TO DO
		left_x_min = leftx_current - margin
		left_x_max = min(midpoint//2 * 3, leftx_current + margin)
		right_x_min = max(midpoint//4 , rightx_current - margin)
		right_x_max = rightx_current + margin

		if left_x_max <= right_x_min and leftx_current > window_width // 2: # intersected on right side
			threshold = 1
		elif left_x_max <= right_x_min and leftx_current < window_width // 2:
			threshold = 1

		# left_x_min = 0
		# left_x_max = midpoint
		# right_x_min = midpoint+1
		# right_x_max = window_width-1
		
		y_min = window*window_height
		y_max = (window+1)*window_height
		
		####
		# Draw the windows on the visualization image using cv2.rectangle()
		##TO DO
		# print(out_img.shape)
		# print(left_x_min)
		# print(left_x_max)
		# print(right_x_min)
		# print(right_x_max)
		# print(y_min)
		# print(y_max)
		# print()
		cv2.rectangle(out_img, (left_x_min, y_min), (left_x_max, y_max), (0, 255, 0), 2)
		cv2.rectangle(out_img, (right_x_min, y_min), (right_x_max, y_max), (0, 255, 0), 2)
		
		# show(out_img)
		####
		# Identify the nonzero pixels in x and y within the window
		##TO DO

		left_filter = np.logical_and((left_x_min <= nonzerox), (nonzerox <= left_x_max))
		left_filter = np.logical_and(left_filter, (nonzeroy >= y_min))
		left_filter = np.logical_and(left_filter, (nonzeroy <= y_max))
		left_points = np.argwhere(left_filter).flatten()
		right_filter = np.logical_and((right_x_min <= nonzerox), (nonzeroy <= right_x_max))
		right_filter = np.logical_and(right_filter, (nonzeroy >= y_min))
		right_filter = np.logical_and(right_filter, (nonzeroy <= y_max))
		right_points = np.argwhere(right_filter).flatten()

		# for i, point in enumerate(zip(nonzerox, nonzeroy)):
		# 	# print(point.shape)
		# 	x, y = point[0], point[1]
		# 	if left_x_min <= x <= left_x_max and y_min <= y <= y_max:
		# 		left_points.append(i)
		# 	if right_x_min <= x <= right_x_max and y_min <= y <= y_max:
		# 		right_points.append(i)

		####
		# Append these indices to the lists
		##TO DO
		if len(left_points) != 0:
			left_lane_inds += list(left_points)
		if len(right_points) != 0:
			right_lane_inds += list(right_points)
		####
		# If you found > minpix pixels, recenter next window on their mean position
		##TO DO
		if len(left_points) > minpix:
			# print(nonzerox[left_points])
			leftx_current = int(max(margin, np.mean(nonzerox[left_points])))
		if	len(right_points) > minpix:
			rightx_current = int(min(out_img.shape[1]-margin, np.mean(nonzerox[right_points])))
		####

	# show(out_img)
	# Concatenate the arrays of indices
	# left_lane_inds = np.concatenate(left_lane_inds)
	# right_lane_inds = np.concatenate(right_lane_inds)

	# Extract left and right line pixel positions
	leftx = nonzerox[left_lane_inds]
	lefty = nonzeroy[left_lane_inds]
	rightx = nonzerox[right_lane_inds]
	righty = nonzeroy[right_lane_inds]

	# Fit a second order polynomial to each using np.polyfit()
	# If there isn't a good fit, meaning any of leftx, lefty, rightx, and righty are empty,
	# the second order polynomial is unable to be sovled.
	# Thus, it is unable to detect edges.
	try:
	##TODO
		# plt.imshow(cv2.cvtColor(out_img, cv2.COLOR_BGR2RGB))
		# plt.plot(leftx, lefty, 'o')
		# plt.plot(rightx, righty, 'o', color='blue')
		# plt.show()
		# threshold = .7
		left_fit = np.polyfit(lefty, leftx, 2)
		# slope, intercept, r_value_l, p_value, std_err = scipy.stats.linregress(leftx, lefty)
		
		# if r_value < threshold:
		# 	left_fit = estimate_left_lane(rightx, righty, 10)
		# right_fit = estimate_right_lane(leftx, lefty, 10)
		right_fit = np.polyfit(righty, rightx, 2)
		# slope, intercept, r_value_r, p_value, std_err = scipy.stats.linregress(rightx, righty)
		# print(f'right: {r_value_r**2} left: {r_value_l**2}')
		# if r_value < threshold:
		# 	right_fit = estimate_right_lane(leftx, lefty, 10)
		# xs = list(leftx) + list(rightx)
		# plt.plot(xs, [left_fit[0]*x**2 + left_fit[1]*x + left_fit[2] for x in xs], label='left lane')
		# plt.plot(xs, [right_fit[0]*x**2 + right_fit[1]*x + right_fit[2] for x in xs], label='right lane')
		# plt.legend()
		# plt.show()
		
		y_thresh = window_height//2
		# print(min(rightx))
		# print(min(leftx))
		_, _, r_l, _, _ = scipy.stats.linregress(leftx, lefty)
		_, _, r_r, _, _ = scipy.stats.linregress(rightx, righty)
		
		# r_l = r_l**2
		# r_r = r_r**2
		r_l = np.abs(r_l)
		r_r = np.abs(r_r)

		# print(f"\033[0;35m{curvature(leftx, lefty)}\033[0m and \033[0;35m{curvature(rightx, righty)}\033[0m")
		
		flag = 0
		# print(f'{max(lefty)} {ma(righty)}')
		if max(lefty) < window_height - y_thresh:
			r_l = 0
		if max(righty) < window_height - y_thresh:
			r_r = 0
		# if r_l > r_r:
		# 	print(f'\033[1;32m{r_l}\033[0m vs \033[0;31m{r_r}\033[0m')
		# else:
		# 	print(f'\033[0;31m{r_l}\033[0m vs \033[1;32m{r_r}\033[0m')
		if r_l > r_r and r_r < threshold:
			# print('left lane better!')
			flag = 1
			right_fit = estimate_right_lane(left_fit, 700)
		elif r_l < threshold:
			# print('right lane better!')
			flag = 2
			left_fit = estimate_left_lane(right_fit, 700)

		if plot:
			plt.scatter(leftx, lefty, c='b')
			plt.scatter(rightx, righty, c='r')
			xs = list(leftx) + list(rightx)
			if flag == 0:
				plt.plot(xs, [left_fit[0]*x**2 + left_fit[1]*x + left_fit[2] for x in xs], label='left lane')
				plt.plot(xs, [right_fit[0]*x**2 + right_fit[1]*x + right_fit[2] for x in xs], label='right lane')
			elif flag == 1:
				plt.plot(xs, [left_fit[0]*x**2 + left_fit[1]*x + left_fit[2] for x in xs], label='left lane', c='g')
				plt.plot(xs, [right_fit[0]*x**2 + right_fit[1]*x + right_fit[2] for x in xs], label='right lane')
			else:
				plt.plot(xs, [left_fit[0]*x**2 + left_fit[1]*x + left_fit[2] for x in xs], label='left lane')
				plt.plot(xs, [right_fit[0]*x**2 + right_fit[1]*x + right_fit[2] for x in xs], label='right lane', c='g')
			plt.legend()
			plt.show()

	####
	except TypeError:
		print("Unable to detect lanes")
	# Fit a second order polynomial to each using np.polyfit()
	# If there isn't a good fit, meaning any of leftx, lefty, rightx, and righty are empty,
	# the second order polynomial is unable to be sovled.
		return None


	# Return a dict of relevant variables
	ret = {}
	ret['left_fit'] = left_fit
	ret['right_fit'] = right_fit
	ret['nonzerox'] = nonzerox
	ret['nonzeroy'] = nonzeroy
	ret['out_img'] = out_img
	ret['left_lane_inds'] = left_lane_inds
	ret['right_lane_inds'] = right_lane_inds

	return ret


def tune_fit(binary_warped, left_fit, right_fit):
	"""
	Given a previously fit line, quickly try to find the line based on previous lines
	"""
	# Assume you now have a new warped binary image
	# from the next frame of video (also called "binary_warped")
	# It's now much easier to find line pixels!
	nonzero = binary_warped.nonzero()
	nonzeroy = np.array(nonzero[0])
	nonzerox = np.array(nonzero[1])
	margin = 100
	left_lane_inds = ((nonzerox > (left_fit[0]*(nonzeroy**2) + left_fit[1]*nonzeroy + left_fit[2] - margin)) & (nonzerox < (left_fit[0]*(nonzeroy**2) + left_fit[1]*nonzeroy + left_fit[2] + margin)))
	right_lane_inds = ((nonzerox > (right_fit[0]*(nonzeroy**2) + right_fit[1]*nonzeroy + right_fit[2] - margin)) & (nonzerox < (right_fit[0]*(nonzeroy**2) + right_fit[1]*nonzeroy + right_fit[2] + margin)))

	# Again, extract left and right line pixel positions
	leftx = nonzerox[left_lane_inds]
	lefty = nonzeroy[left_lane_inds]
	rightx = nonzerox[right_lane_inds]
	righty = nonzeroy[right_lane_inds]

	r_l = np.corrcoef(leftx, lefty)[0,1]
	r_r = np.corrcoef(rightx, righty)[0,1]
	# r_l = r_l**2
	# r_r = r_r**2
	threshold = 0.8
	r_l = np.abs(r_l)
	r_r = np.abs(r_r)
	# if r_l > r_r:
	# 	print(f'TUNE FIT: \033[1;32m{r_l}\033[0m vs \033[0;31m{r_r}\033[0m')
	# else:
	# 	print(f'TUNE FIT: \033[0;31m{r_l}\033[0m vs \033[1;32m{r_r}\033[0m')

	if r_l < threshold or r_r < threshold:
		# print('\033[1;32m LINE BAD!!!!!!!! \033[0m')
		return "floppa"
		
		

	# If we don't find enough relevant points, return all None (this means error)
	min_inds = 10
	if lefty.shape[0] < min_inds or righty.shape[0] < min_inds:
		return None

	# Fit a second order polynomial to each
	left_fit = np.polyfit(lefty, leftx, 2)
	right_fit = np.polyfit(righty, rightx, 2)
	# Generate x and y values for plotting
	ploty = np.linspace(0, binary_warped.shape[0]-1, binary_warped.shape[0] )
	left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
	right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]

	# Return a dict of relevant variables
	ret = {}
	ret['left_fit'] = left_fit
	ret['right_fit'] = right_fit
	ret['nonzerox'] = nonzerox
	ret['nonzeroy'] = nonzeroy
	ret['left_lane_inds'] = left_lane_inds
	ret['right_lane_inds'] = right_lane_inds

	return ret


def viz1(binary_warped, ret, save_file=None):
	"""
	Visualize each sliding window location and predicted lane lines, on binary warped image
	save_file is a string representing where to save the image (if None, then just display)
	"""
	# Grab variables from ret dictionary
	left_fit = ret['left_fit']
	right_fit = ret['right_fit']
	nonzerox = ret['nonzerox']
	nonzeroy = ret['nonzeroy']
	out_img = ret['out_img']
	left_lane_inds = ret['left_lane_inds']
	right_lane_inds = ret['right_lane_inds']

	# Generate x and y values for plotting
	ploty = np.linspace(0, binary_warped.shape[0]-1, binary_warped.shape[0] )
	left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
	right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]

	out_img[nonzeroy[left_lane_inds], nonzerox[left_lane_inds]] = [255, 0, 0]
	out_img[nonzeroy[right_lane_inds], nonzerox[right_lane_inds]] = [0, 0, 255]
	plt.imshow(out_img)
	plt.plot(left_fitx, ploty, color='yellow')
	plt.plot(right_fitx, ploty, color='yellow')
	plt.xlim(0, 1280)
	plt.ylim(720, 0)
	if save_file is None:
		plt.show()
	else:
		plt.savefig(save_file)
	plt.gcf().clear()


def bird_fit(binary_warped, ret, save_file=None):
	"""
	Visualize the predicted lane lines with margin, on binary warped image
	save_file is a string representing where to save the image (if None, then just display)
	"""
	# Grab variables from ret dictionary
	left_fit = ret['left_fit']
	right_fit = ret['right_fit']
	nonzerox = ret['nonzerox']
	nonzeroy = ret['nonzeroy']
	left_lane_inds = ret['left_lane_inds']
	right_lane_inds = ret['right_lane_inds']

	# Create an image to draw on and an image to show the selection window
	out_img = (np.dstack((binary_warped, binary_warped, binary_warped))*255).astype('uint8')
	window_img = np.zeros_like(out_img)
	# Color in left and right line pixels
	out_img[nonzeroy[left_lane_inds], nonzerox[left_lane_inds]] = [255, 0, 0]
	out_img[nonzeroy[right_lane_inds], nonzerox[right_lane_inds]] = [0, 0, 255]

	# Generate x and y values for plotting
	ploty = np.linspace(0, binary_warped.shape[0]-1, binary_warped.shape[0])
	left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
	right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]

	# Generate a polygon to illustrate the search window area
	# And recast the x and y points into usable format for cv2.fillPoly()
	margin = 100  # NOTE: Keep this in sync with *_fit()
	left_line_window1 = np.array([np.transpose(np.vstack([left_fitx-margin, ploty]))])
	left_line_window2 = np.array([np.flipud(np.transpose(np.vstack([left_fitx+margin, ploty])))])
	left_line_pts = np.hstack((left_line_window1, left_line_window2))
	right_line_window1 = np.array([np.transpose(np.vstack([right_fitx-margin, ploty]))])
	right_line_window2 = np.array([np.flipud(np.transpose(np.vstack([right_fitx+margin, ploty])))])
	right_line_pts = np.hstack((right_line_window1, right_line_window2))

	# Draw the lane onto the warped blank image
	cv2.fillPoly(window_img, np.int_([left_line_pts]), (0,255, 0))
	cv2.fillPoly(window_img, np.int_([right_line_pts]), (0,255, 0))
	result = cv2.addWeighted(out_img, 1, window_img, 0.3, 0)
	
	# print("In bird fit function")
	# plt.imshow(result)
	# plt.plot(left_fitx, ploty, color='yellow')
	# plt.plot(right_fitx, ploty, color='yellow')
	# plt.xlim(0, 1280)
	# plt.ylim(720, 0)
	# plt.show()


	return result


def final_viz(undist, left_fit, right_fit, m_inv):
	"""
	Final lane line prediction visualized and overlayed on top of original image
	"""
	# Generate x and y values for plotting
	ploty = np.linspace(0, undist.shape[0]-1, undist.shape[0])
	left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
	right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]

	# Create an image to draw the lines on
	#warp_zero = np.zeros_like(warped).astype(np.uint8)
	#color_warp = np.dstack((warp_zero, warp_zero, warp_zero))
	color_warp = np.zeros((720, 1280, 3), dtype='uint8')  # NOTE: Hard-coded image dimensions
	y_val = 90
	front_midpoint = (left_fitx[y_val] + right_fitx[y_val]) // 2
	
	# left_x_midpoint = left_fitx[0]
	# right_x_midpoint = right_fitx[0]
	
	# Recast the x and y points into usable format for cv2.fillPoly()
	pts_left = np.array([np.transpose(np.vstack([left_fitx, ploty]))])
	pts_right = np.array([np.flipud(np.transpose(np.vstack([right_fitx, ploty])))])
	pts = np.hstack((pts_left, pts_right))

	# Draw the lane onto the warped blank image
	cv2.fillPoly(color_warp, np.int_([pts]), (0,255, 0))
	# cv2.circle(color_warp, (int(left_x_midpoint), 0), 20, (0,0,255), -1)
	# cv2.circle(color_warp, (int(right_x_midpoint), 0), 20, (0,0,255), -1)
	cv2.circle(color_warp, (int(front_midpoint), y_val), 20, (0,0,255), -1)
	# print(front_midpoint)

	# Warp the blank back to original image space using inverse perspective matrix (Minv)
	newwarp = cv2.warpPerspective(color_warp, m_inv, (undist.shape[1], undist.shape[0]))
	# Combine the result with the original image
	# Convert arrays to 8 bit for later cv to ros image transfer
	undist = np.array(undist, dtype=np.uint8)
	newwarp = np.array(newwarp, dtype=np.uint8)
	result = cv2.addWeighted(undist, 1, newwarp, 0.3, 0)

	return result


# print("In bird fit function")
# result = bird_fit()
# img = cv2.imread("b.png")
# plt.imshow(result)
# plt.plot(left_fitx, ploty, color='yellow')
# plt.plot(right_fitx, ploty, color='yellow')
# plt.xlim(0, 1280)
# plt.ylim(720, 0)
# plt.show()