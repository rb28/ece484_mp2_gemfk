import time
import math
import numpy as np
import cv2
import rospy
# import alvinxy.alvinxy as axy # Import AlvinXY transformation module

from line_fit import line_fit, tune_fit, bird_fit, final_viz
from Line import Line
from sensor_msgs.msg import Image
from novatel_gps_msgs.msg import Inspva
from ackermann_msgs.msg import AckermannDrive
from std_msgs.msg import Header
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Float32, Float32MultiArray
from skimage import morphology
import matplotlib.pyplot as plt
import pandas as pd

# from line_profiler import LineProfiler


# plt.plot(x.values, y.values, 'ro', c='black')
# plt.pause(0.00000001)

class lanenet_detector():
    def __init__(self):

        self.bridge = CvBridge()
        # NOTE
        # Uncomment this line for lane detection of GEM car in Gazebo
        # self.sub_image = rospy.Subscriber('/gem/front_single_camera/front_single_camera/image_raw', Image, self.img_callback, queue_size=1)
        # Uncomment this line for lane detection of videos in rosbag
        # self.sub_image = rospy.Subscriber('camera/image_raw', Image, self.img_callback, queue_size=1) # ?bag
        self.sub_image = rospy.Subscriber('/zed2/zed_node/rgb/image_rect_color', Image, self.img_callback, queue_size=1) #0830
        # self.sub_coords = rospy.Subscriber('/novatel/inspva', Inspva, self.gnss_callback, queue_size=1)
        # self.sub_image = rospy.Subscriber('camera/image_raw', Image, self.img_callback, queue_size=1) # bag: 0011
        self.pub_image = rospy.Publisher("lane_detection/annotate", Image, queue_size=1)
        self.pub_bird = rospy.Publisher("lane_detection/birdseye", Image, queue_size=1)
        self.pub_debug = rospy.Publisher("debug/debug_image", Image, queue_size=1)
        self.pub_color_debug = rospy.Publisher("debug/debug_color", Image, queue_size=1)
        self.pub_debug_coordinates = rospy.Publisher("debug/debug_coordinates", Image, queue_size=1)
        self.pub_next_waypoint = rospy.Publisher("debug/next_waypoint", Float32MultiArray, queue_size=1)
        self.pub_debug_sobel = rospy.Publisher("debug/debug_sobel", Image, queue_size=1)
        self.pub_debug_combined = rospy.Publisher("debug/debug_combined", Image, queue_size=1)

        self.sub_next_waypoint = rospy.Subscriber("debug/next_waypoint", Float32MultiArray, self.waypoint_callback, queue_size=1)

        self.stanley_pub = rospy.Publisher('/gem/stanley_gnss_cmd', AckermannDrive, queue_size=1)
        self.ackermann_msg                         = AckermannDrive()
        self.ackermann_msg.steering_angle_velocity = 0.0
        self.ackermann_msg.acceleration            = 0.0
        self.ackermann_msg.jerk                    = 0.0
        self.ackermann_msg.speed                   = 0.0 
        self.ackermann_msg.steering_angle          = 0.0

        self.left_line = Line(n=5)
        self.right_line = Line(n=5)
        self.detected = False
        self.hist = True
        self.timer = -1
        self.gnss_coords = None

        

    # ripped from alvinxy, couldnt figure out how to install it easily
    def ll2xy(self, lat, lon):
        olat       = 40.0928563
        olon       = -88.2359994
        latrad = lat * 2.0 * np.pi / 360.0
        dx = 111415.13 * np.cos(latrad) \
            - 94.55 * np.cos(3.0 * latrad) \
            + 0.12 * np.cos(5.0 * latrad)
        dy = 111132.09 - 566.05 * np.cos(2.0 * latrad) \
            + 1.20 * np.cos(4.0 * latrad) \
            - 0.002 * np.cos(6.0 * latrad)
        x = (lon - olon) * dx
        y = (lat - olat) * dy
        return x, y

    def pixels_to_meters(self, dist):
        return dist * 10/700 * 0.3048

    def img_callback(self, data):

        try:
            # Convert a ROS image message into an OpenCV image
            # print('fsdfsfsd')
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        raw_img = cv_image.copy()
        mask_image, bird_image = self.detection(raw_img)
        # print(mask_image is None)
        # print(bird_image is None)
        # print()
        # print("Outside if statement")
        if mask_image is not None and bird_image is not None:
            # print("INSIDE if statement")
            # Convert an OpenCV image into a ROS image message
            out_img_msg = self.bridge.cv2_to_imgmsg(mask_image, 'bgr8')
            out_bird_msg = self.bridge.cv2_to_imgmsg(bird_image, 'bgr8')

            # Publish image message in ROS
            self.pub_image.publish(out_img_msg)
            self.pub_bird.publish(out_bird_msg)

    # def gnss_callback(self, data):
    #     # print(data.azimuth % 360)
    #     self.gnss_coords = (data.latitude, data.longitude, np.radians(data.azimuth % 360))
    #     # print(data.latitude, data.longitude)

    def waypoint_callback(self, data):
        print(data.data)

    def gradient_thresh(self, img, thresh_min=25, thresh_max=100):
        """
        Apply sobel edge detection on input image in x, y direction
        """
        #1. Convert the image to gray scale
        #2. Gaussian blur the image
        #3. Use cv2.Sobel() to find derievatives for both X and Y Axis
        #4. Use cv2.addWeighted() to combine the results
        #5. Convert each pixel to unint8, then apply threshold to get binary image

        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_blur = cv2.GaussianBlur(img_gray, (5, 5), 0)
        grad_x = cv2.Sobel(img_blur, cv2.CV_64F, 1, 0, ksize=3)
        grad_y = cv2.Sobel(img_blur, cv2.CV_64F, 0, 1, ksize=3)
        abs_grad_x = cv2.convertScaleAbs(grad_x)
        abs_grad_y = cv2.convertScaleAbs(grad_y)
        combined_grad = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)
        binary_output = np.where((thresh_min <= combined_grad) & (combined_grad <= thresh_max), 255, 0)
        # binary_output = np.array([[1 if x[i]>=thresh_min and x[i]<=thresh_max else 0 for i in range(len(x))] for x in combined_grad])
        out_img = self.bridge.cv2_to_imgmsg(binary_output.astype(np.uint8), '8UC1')
        self.pub_debug_sobel.publish(out_img)

        return binary_output.astype(np.uint8)

    def color_thresh(self, img, thresh=(100, 255)):
        """
        Convert RGB to HSL and threshold to binary image using S channel
        """
        #1. Convert the image from RGB to HSL
        #2. Apply threshold on S channel to get binary image
        #Hint: threshold on H to remove green grass
        
        # img_hsl = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)
        # green_thresh = (90, 200)
        # for i in range(img_s_filtered.shape[0]):
        #     for j in range(img_s_filtered.shape[1]):
        #         curr_s = img_s_filtered[2]
        # green_filtered = np.array([[[0, 0, 0] if (p[0] >= green_thresh[0] and p[0] <= green_thresh[1]) else p for p in row] for row in img_hsl])


        # binary_output = np.array([[255 if (p[2] >= thresh[0] and p[2] <= thresh[1]) else 0 for p in row] for row in img_hsl])
        # return binary_output.astype(np.uint8)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)
        green_thresh = (50/360 * 255, 155/360 * 255)
        orange_thresh = (160 / 360 * 255, 300 / 360 * 255)
        # no_green = img.copy()
        # for i in range(no_green.shape[0]):
        #         for j in range(no_green.shape[1]):
        #                 curr_point = no_green[i, j]
        #                 if curr_point[0] > green_thresh[0] and curr_point[1] < green_thresh[1]:
        #                         no_green[i, j] = np.array([0, 0, 0])
        # no_green = np.where((img[:,:,0] >= green_thresh[0]) & (img[:,:,0] <= green_thresh[1]), np.array([0,0,0]), img)
        # no_green = cv2.cvtColor(no_green, cv2.COLOR_HLS2BGR)
        # image_final = no_green.copy()
        # ret, h_filtered_1 = cv2.threshold(img[:,:,0], green_thresh[0], green_thresh[1], cv2.THRESH_BINARY)
        # img[:,:,0] = h_filtered_1
        # ret, h_filtered_2 = cv2.threshold(img[:,:,0], orange_thresh[0], orange_thresh[1], cv2.THRESH_BINARY_INV)
        # img[:,:,0] = h_filtered_2
        # ret, h_filtered_3 = cv2.threshold(img[:,:,1], 200, thresh[1], cv2.THRESH_BINARY)
        # img[:,:,0] = h_filtered_3
        # ret, s_filtered = cv2.threshold(img[:,:,2], thresh[0], thresh[1], cv2.THRESH_BINARY)
        # img[:,:,2] = s_filtered

        lw = np.array([30, 175, 0])
        uw = np.array([255, 255, 255])

        white = cv2.inRange(img, lw, uw)
        img = cv2.bitwise_and(img, img, mask=white)

        # ly = np.array([30, 0, 0])
        # uy = np.array([90, 0, 0])

        # yellow = cv2.inRange(img, lw, uw)
        # yellow = cv2.bitwise_not(yellow)
        # img = cv2.bitwise_and(img, img, mask=yellow)
        
        # img = cv2.bitwise_or(img, img2)

        img = cv2.cvtColor(img, cv2.COLOR_HLS2BGR)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        out_img = self.bridge.cv2_to_imgmsg(img, '8UC1')
        self.pub_color_debug.publish(out_img)
        # print(image_final.shape)
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        return img


    
    def combinedBinaryImage(self, img):
        """
        Get combined binary image from color filter and sobel filter
        """
        #1. Apply sobel filter and color filter on input image
        #2. Combine the outputs
        ## Here you can use as many methods as you want.

        ## TODO
        SobelOutput = self.gradient_thresh(img)

        # Use for bag 0056
        # ColorOutput = self.color_thresh(img, thresh=(150, 255))

        # Use for everything else
        ColorOutput = self.color_thresh(img, thresh=(70, 255))
        # print(SobelOutput.shape)
        # print(ColorOutput.shape)
        binaryImage = np.zeros_like(ColorOutput)
        # cv2.imshow(ColorOutput, 'afsd')
        # cv2.waitKey(0)
        binaryImage[(ColorOutput>=150)&(SobelOutput==255)] = 255
        out_img = self.bridge.cv2_to_imgmsg(binaryImage, '8UC1')
        self.pub_debug_combined.publish(out_img)
        # result = cv2.bitwise_and(SobelOutput, ColorOutput)
        # Remove noise from binary image
        binaryImage = morphology.remove_small_objects(ColorOutput.astype("bool"),min_size=50,connectivity=2)
        # plt.imshow(binaryImage)
        # plt.show()
        
        return binaryImage


    def perspective_transform(self, img, verbose=False):
        """
        Get bird's eye view from input image
        """
        #1. Visually determine 4 source points and 4 destination points
        #2. Get M, the transform matrix, and Minv, the inverse using cv2.getPerspectiveTransform()
        #3. Generate warped image in bird view using cv2.warpPerspective()

        

        # bottom left, bottom right, top left, top right

        # Bag 0830 source points
        # src = np.float32([[55, 710], [1100, 710], [500, 380], [715, 380]])

        # Bag 0011 source points
        # src = np.float32([[320, 365], [810, 365], [565, 200], [685, 200]])

        # Bag 484 source points
        # src = np.float32([[400, 365], [870, 365], [600, 270], [740, 270]])

        # Bag 0056 src pts
        # src = np.float32([[200, 365], [810, 365], [500, 220], [685, 220]])

        # Bag GEM src pts
        src = np.float32([[50, 700], [1200, 700], [300, 500], [950, 500]])

        # Simulation source points
        # src = np.float32([[5, 430], [630, 430], [215, 295], [450, 295]])

        dst =  np.float32([[0, img.shape[0]], [img.shape[1], img.shape[0]], [0, 0], [img.shape[1], 0]])
        M = cv2.getPerspectiveTransform(src, dst)

        Minv = np.linalg.inv(M)
        warped_img = cv2.warpPerspective(np.float32(img), M, (img.shape[1], img.shape[0]), flags=cv2.INTER_LINEAR)
        # # print(warped_img.shape)
        # plt.imshow(warped_img)
        # plt.show()
        # cv2.imwrite("b.png", img)

        return warped_img, M, Minv
    
    def get_real_coordinates(self, point_on_image, Minv):

        point_homogeneous = np.array([point_on_image[0], point_on_image[1], 1])

        real_coordinates_homogeneous = np.dot(Minv, point_homogeneous)
        
        real_coordinates = real_coordinates_homogeneous / real_coordinates_homogeneous[2]

        return real_coordinates[:2] 
    
    def local_to_global(self, local_point, car_position, azimuth_rad):
        # azimuth_rad = math.radians(azimuth)

        rotation_matrix = [[math.cos(azimuth_rad), -math.sin(azimuth_rad)],
                        [math.sin(azimuth_rad), math.cos(azimuth_rad)]]

        global_point = [car_position[0] + rotation_matrix[0][0]*local_point[0] + rotation_matrix[0][1]*local_point[1],
                        car_position[1] + rotation_matrix[1][0]*local_point[0] + rotation_matrix[1][1]*local_point[1]]

        return global_point

    def get_next_point(self, undist, left_fit, right_fit):
        ploty = np.linspace(0, undist.shape[0]-1, undist.shape[0])
        left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
        right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]
        y_val = 90
        front_midpoint = (left_fitx[y_val] + right_fitx[y_val]) // 2
        return (int(front_midpoint), y_val)
    
    def front2steer(self, f_angle):
        if(f_angle > 35):
            f_angle = 35
        if (f_angle < -35):
            f_angle = -35
        if (f_angle > 9):
            steer_angle = round(-0.1084*f_angle**2 + 21.775*f_angle, 2)
        elif (f_angle < 9):
            f_angle = -f_angle
            steer_angle = -round(-0.1084*f_angle**2 + 21.775*f_angle, 2)
        else:
            steer_angle = 0.0
        return steer_angle

    def detection(self, img):
        # cv2.imwrite("allah.png", img)

        binary_img = self.combinedBinaryImage(img)
        img_birdeye, M, Minv = self.perspective_transform(binary_img)

        if not self.hist:
            # Fit lane without previous result
            ret = line_fit(img_birdeye)
            left_fit = ret['left_fit']
            right_fit = ret['right_fit']
            nonzerox = ret['nonzerox']
            nonzeroy = ret['nonzeroy']
            left_lane_inds = ret['left_lane_inds']
            right_lane_inds = ret['right_lane_inds']

        else:
            # Fit lane with previous result
            self.timer += 1
            if not self.detected or self.timer % 5 == 0:
                ret = line_fit(img_birdeye)

                if ret is not None:
                    left_fit = ret['left_fit']
                    right_fit = ret['right_fit']
                    nonzerox = ret['nonzerox']
                    nonzeroy = ret['nonzeroy']
                    left_lane_inds = ret['left_lane_inds']
                    right_lane_inds = ret['right_lane_inds']
                    out_img = ret['out_img']
                    left_fit = self.left_line.add_fit(left_fit)
                    right_fit = self.right_line.add_fit(right_fit)
                    out_img_msg = self.bridge.cv2_to_imgmsg(out_img, 'bgr8')
                    self.pub_debug.publish(out_img_msg)
                    self.detected = True

            else:
                left_fit = self.left_line.get_fit()
                right_fit = self.right_line.get_fit()
                ret = tune_fit(img_birdeye, left_fit, right_fit)
                if type(ret) == str:
                    ret = line_fit(img_birdeye)
                if ret is not None:
                    left_fit = ret['left_fit']
                    right_fit = ret['right_fit']
                    nonzerox = ret['nonzerox']
                    nonzeroy = ret['nonzeroy']
                    left_lane_inds = ret['left_lane_inds']
                    right_lane_inds = ret['right_lane_inds']

                    left_fit = self.left_line.add_fit(left_fit)
                    right_fit = self.right_line.add_fit(right_fit)

                else:
                    self.detected = False


            # Annotate original image
            bird_fit_img = None
            combine_fit_img = None
            if ret is not None:
                # car_x, car_y = self.ll2xy(self.gnss_coords[0], self.gnss_coords[1])
                binary_warped, _, Minv = self.perspective_transform(img, verbose=False)
                window_height = int(binary_warped.shape[0])
                window_width = int(binary_warped.shape[1])
                car_x = window_width//2
                car_y = window_height
                point_x, point_y = self.get_next_point(binary_warped, left_fit, right_fit)
                # cv2.circle(binary_warped, (int(warped_point_x), int(warped_point_y)), 10, (0, 0, 255), -1)
                # self.pub_debug_coordinates.publish(self.bridge.cv2_to_imgmsg(binary_warped, '32FC3'))
                # point_x, point_y = self.get_real_coordinates((warped_point_x, warped_point_y), Minv)
                # plot point_x, point_y on original image
                # cv2.circle(img, (int(point_x), int(point_y)), 10, (0, 0, 255), -1)
                # self.pub_debug_coordinates.publish(self.bridge.cv2_to_imgmsg(img, 'bgr8'))

                x_offset = self.pixels_to_meters(point_x - car_x)
                y_offset = self.pixels_to_meters(car_y - point_y)

                f_delta = np.arctan2(x_offset, y_offset)
                f_delta = round(np.clip(f_delta, -0.61, 0.61), 3)
                f_delta_deg = np.degrees(f_delta)
                # print(x_offset, y_offset, f_delta_deg)
                steering_angle = self.front2steer(f_delta_deg)
                
                # curr_x, curr_y = self.ll2xy(self.gnss_coords[0], self.gnss_coords[1])
                # new_x, new_y = self.local_to_global((x_offset, y_offset), (curr_x, curr_y), self.gnss_coords[2])
                # rotation_matrix = np.array([[np.cos(self.gnss_coords[2]), -np.sin(self.gnss_coords[2])], [np.sin(self.gnss_coords[2]), np.cos(self.gnss_coords[2])]])
                
                # offset_x = x_offset - self.pixels_to_meters(car_x)
                # offset_y = y_offset - self.pixels_to_meters(car_y)
                # print(offset_x, offset_y)
                # angle_offset = np.arctan2(offset_x, offset_y) * 180 / np.pi
                
                # if self.timer == 0:
                #     f = pd.read_csv('acc_waypoints.csv', header=None)
                #     x = f[0]
                #     y = f[1]
                #     heading = f[2]
                #     plt.xlim(-20,75)
                #     plt.ylim(-20,75)
                #     plt.plot(x.values, y.values, 'ro', c='black', markersize=1)

                # plt.scatter(curr_x, curr_y, c='blue')
                # new_coords = rotation_matrix @ np.array([x_offset, y_offset])
                # x_offset = new_coords[0]
                # y_offset = new_coords[1]
                # curr_x += x_offset
                # curr_y += y_offset
                # plt.scatter(new_coords[0], new_coords[1], c='red')
                # plt.scatter(new_x, new_y, c='red')
                # float32_array = Float32MultiArray()
                # float32_array.data = [new_x, new_y]
                # self.pub_next_waypoint.publish(float32_array)
                
                self.ackermann_msg.acceleration   = 0.35
                self.ackermann_msg.steering_angle = round(steering_angle,1)
                # print(self.ackermann_msg.steering_angle)
                self.stanley_pub.publish(self.ackermann_msg)
                plt.pause(0.00000001)
                bird_fit_img = bird_fit(img_birdeye, ret, save_file=None)
                combine_fit_img = final_viz(img, left_fit, right_fit, Minv)
            else:
                print("Unable to detect lanes")

            return combine_fit_img, bird_fit_img


if __name__ == '__main__':
    # init args
    rospy.init_node('lanenet_node', anonymous=True)
    lanenet_detector()
    while not rospy.core.is_shutdown():
        rospy.rostime.wallsleep(0.5)
