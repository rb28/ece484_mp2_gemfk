import rospy
from gazebo_msgs.srv import GetModelState, GetModelStateResponse
from gazebo_msgs.msg import ModelState
from ackermann_msgs.msg import AckermannDrive
import numpy as np
from std_msgs.msg import Float32MultiArray
import math
from util import euler_to_quaternion, quaternion_to_euler
import time


# infile = open("car_pose.txt", "w")
max_vel = 16
lookahead = 5
angle_power = 2

def lerp_(x1, x2, y1, y2, x):
    return y1 + ((y2 - y1) / (x2 - x1)) * (x - x1)

class vehicleController():

    def __init__(self):
        # Publisher to publish the control input to the vehicle model
        self.controlPub = rospy.Publisher("/ackermann_cmd", AckermannDrive, queue_size = 1)
        self.prev_vel = 0
        self.L = 1.75 # Wheelbase, can be get from gem_control.py
        self.log_acceleration = False

    def getModelState(self):
        # Get the current state of the vehicle
        # Input: None
        # Output: ModelState, the state of the vehicle, contain the
        #   position, orientation, linear velocity, angular velocity
        #   of the vehicle
        rospy.wait_for_service('/gazebo/get_model_state')
        try:
            serviceResponse = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            resp = serviceResponse(model_name='gem')
        except rospy.ServiceException as exc:
            rospy.loginfo("Service did not process request: "+str(exc))
            resp = GetModelStateResponse()
            resp.success = False
        return resp


    # Tasks 1: Read the documentation https://docs.ros.org/en/fuerte/api/gazebo/html/msg/ModelState.html
    #       and extract yaw, velocity, vehicle_position_x, vehicle_position_y
    # Hint: you may use the the helper function(quaternion_to_euler()) we provide to convert from quaternion to euler
    def extract_vehicle_info(self, currentPose):

        ####################### TODO: Your TASK 1 code starts Here #######################
        pos_x, pos_y, vel, yaw = 0, 0, 0, 0
        model_state = self.getModelState()
        # x, y, _, w = model_state.pose.orientation
        _, _, yaw = quaternion_to_euler(model_state.pose.orientation.x, model_state.pose.orientation.y, model_state.pose.orientation.z, model_state.pose.orientation.w)
        pos_x, pos_y = model_state.pose.position.x, model_state.pose.position.y
        vel = np.linalg.norm(np.array([model_state.twist.linear.x, model_state.twist.linear.y, model_state.twist.linear.z]))
        ####################### TODO: Your Task 1 code ends Here #######################
        # print(f"pos_x: {pos_x}, pos_y: {pos_y}, vel: {vel}, yaw: {yaw}")
        return pos_x, pos_y, vel, yaw # note that yaw is in radian

    # Task 2: Longtitudal Controller
    # Based on all unreached waypoints, and your current vehicle state, decide your velocity
    def longititudal_controller(self, curr_x, curr_y, curr_vel, curr_yaw, future_unreached_waypoints):

        ####################### TODO: Your TASK 2 code starts Here #######################

        point = future_unreached_waypoints[lookahead] if len(future_unreached_waypoints) > lookahead else future_unreached_waypoints[-1]

        dx = point[0] - curr_x
        dy = point[1] - curr_y



        min_vel = 8

        angle = 0
        target_velocity = 0

        deadband = 0.1
        # print(f"dx: {dx}, dy: {dy}")
        if np.abs(dx) < deadband or np.abs(dy) < deadband:
            target_velocity = max_vel
        else:
            angle = np.abs(np.arctan2(dy, dx))
            target_velocity = lerp_(0, np.pi, max_vel, min_vel, np.clip(angle_power*angle, 0, np.pi))

        # print(f"angle: {angle}, vel: {target_velocity}")
        
        ####################### TODO: Your TASK 2 code ends Here #######################
        return target_velocity


    # Task 3: Lateral Controller (Pure Pursuit)
    def pure_pursuit_lateral_controller(self, curr_x, curr_y, curr_yaw, target_point, future_unreached_waypoints):

        ####################### TODO: Your TASK 3 code starts Here #######################
        K_dd = 0.75
        min_ld = 4
        max_ld = (lookahead+1)*11.965374026526924
        _, _, vel, _ = self.extract_vehicle_info(self.getModelState())

        l_d = np.clip(K_dd * vel, min_ld, max_ld)
        
        ideal = int(l_d/11.965374026526924)
        index = ideal if len(future_unreached_waypoints) > ideal else -1
        point_1 = np.array(future_unreached_waypoints[index])
        # point_2 = np.array(future_unreached_waypoints[int(l_d/11.965374026526924)+1])

        # target_point = (point_1 + point_2) / 2
        target_point = point_1

        target_line = np.array([target_point[0]-curr_x, target_point[1]-curr_y])
        curr_heading = np.array([curr_x * np.cos(curr_yaw), curr_y * np.sin(curr_yaw)])

        # l_d = np.linalg.norm(target_line, 2)
        
        alpha = np.arctan2(target_line[1], target_line[0]) - curr_yaw

        target_steering = np.arctan2((2*self.L * np.sin(alpha)), l_d)

        ####################### TODO: Your TASK 3 code starts Here #######################
        return target_steering


    def execute(self, currentPose, target_point, future_unreached_waypoints):
        # Compute the control input to the vehicle according to the
        # current and reference pose of the vehicle
        # Input:
        #   currentPose: ModelState, the current state of the vehicle
        #   target_point: [target_x, target_y]
        #   future_unreached_waypoints: a list of future waypoints[[target_x, target_y]]
        # Output: None

        curr_x, curr_y, curr_vel, curr_yaw = self.extract_vehicle_info(currentPose)

        # Acceleration Profile
        if self.log_acceleration:
            acceleration = (curr_vel- self.prev_vel) * 100 # Since we are running in 100Hz


        # self.prev_vel = curr_vel
        target_velocity = self.longititudal_controller(curr_x, curr_y, curr_vel, curr_yaw, future_unreached_waypoints)
        target_steering = self.pure_pursuit_lateral_controller(curr_x, curr_y, curr_yaw, target_point, future_unreached_waypoints)


        #Pack computed velocity and steering angle into Ackermann command
        newAckermannCmd = AckermannDrive()
        newAckermannCmd.speed = target_velocity
        newAckermannCmd.steering_angle = target_steering
        # infile.write(f"{currentPose.pose.position.x} {currentPose.pose.position.y} {acceleration}\n")
        # Publish the computed control input to vehicle model
        self.controlPub.publish(newAckermannCmd)

    def stop(self):
        newAckermannCmd = AckermannDrive()
        newAckermannCmd.speed = 0
        self.controlPub.publish(newAckermannCmd)
